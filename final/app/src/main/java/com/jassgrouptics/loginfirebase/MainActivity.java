package com.jassgrouptics.loginfirebase;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText  edtCorreo, edtClave;
    private Button btnIniciarSesion, btnCancelar;

    private FirebaseAuth mAuth;

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtCorreo = findViewById(R.id.edtCorreo);
        edtClave = findViewById(R.id.edtClave);

        btnIniciarSesion = findViewById(R.id.btnIniciarSesion);
        btnCancelar = findViewById(R.id.btnCancelar);

        btnIniciarSesion.setOnClickListener(this);
        btnCancelar.setOnClickListener(this);

        mAuth = FirebaseAuth.getInstance();

        context = this;

    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();

        if(currentUser!=null){
            //esta logeado
        }else{
            //no esta loegado
        }
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btnIniciarSesion){
            String email, password;
            email = edtCorreo.getText().toString();
            password = edtClave.getText().toString();

            loginIn(email,password);

        }else if(v.getId() == R.id.btnCancelar){
            finish();
        }
    }

    private void loginIn(String email, String password){

        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("Login", "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            Toast.makeText(MainActivity.this, "Login Exitoso",
                                    Toast.LENGTH_SHORT).show();

                            Intent intent = new Intent(MainActivity.this,ProfileActivity.class);
                            context.startActivity(intent);

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("Login", "signInWithEmail:failure", task.getException());
                            Toast.makeText(MainActivity.this, "Error al intentar logearse",
                                    Toast.LENGTH_SHORT).show();

                        }

                        // ...
                    }
                });

    }
}
