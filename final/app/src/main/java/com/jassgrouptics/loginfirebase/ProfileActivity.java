package com.jassgrouptics.loginfirebase;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class ProfileActivity extends AppCompatActivity {

    private TextView txtApenom, txtCorreo, txtTelefono, txtCiudad;

    Context context;
    private FirebaseAuth firebaseAuth;
    DatabaseReference databaseReference;
    private FirebaseUser MiUser;
    private String id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        txtApenom = findViewById(R.id.txtApenom);
        txtCorreo = findViewById(R.id.txtCorreo);
        txtTelefono = findViewById(R.id.txtTelefono);
        txtCiudad = findViewById(R.id.txtCiudad);


        firebaseAuth=FirebaseAuth.getInstance();

        FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();

        id = firebaseUser.getUid();

        databaseReference = FirebaseDatabase.getInstance().getReference();

        context = this;


        databaseReference.child("usuarios").child(id).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //data = dataSnapshot.getValue().toString();

                Usuario usuario = dataSnapshot.getValue(Usuario.class);

                txtApenom.setText(usuario.getApellidos() + ", "+usuario.getNombres());
                txtCorreo.setText(usuario.getCorreo());
                txtTelefono.setText(usuario.getTelefono());
                txtCiudad.setText(usuario.getCiudad());

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {


            }
        });


    }
}
