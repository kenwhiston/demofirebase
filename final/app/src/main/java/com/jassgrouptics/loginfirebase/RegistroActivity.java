package com.jassgrouptics.loginfirebase;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class RegistroActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText edtApellidos, edtNombres, edtCorreo, edtClave, edtRepetirClave;
    private Button btnGuardar,btnCancelar;

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        edtApellidos = findViewById(R.id.edtApellidos);
        edtNombres = findViewById(R.id.edtNombres);
        edtCorreo = findViewById(R.id.edtCorreo);
        edtClave = findViewById(R.id.edtClave);
        edtRepetirClave = findViewById(R.id.edtRepetirClave);

        btnGuardar = findViewById(R.id.btnGuardar);
        btnCancelar = findViewById(R.id.btnCancelar);

        btnGuardar.setOnClickListener(this);
        btnCancelar.setOnClickListener(this);

        mAuth = FirebaseAuth.getInstance();

    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();

        if(currentUser!=null){
            //esta logeado
        }else{
            //no esta loegado
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnGuardar:
                String apellidos, nombres, clave, correo;
                apellidos = edtApellidos.getText().toString();
                nombres = edtNombres.getText().toString();
                clave = edtClave.getText().toString();
                correo = edtCorreo.getText().toString();
                registro(apellidos, nombres, correo, clave);
                break;
            case R.id.btnCancelar:

                break;
        }
    }

    private void registro(String apellidos, String nombres, String correo, String clave){

        mAuth.createUserWithEmailAndPassword(correo, clave)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("Registro", "createUserWithEmail:success");
                            Toast.makeText(RegistroActivity.this, "Registro exitoso",
                                    Toast.LENGTH_SHORT).show();
                            FirebaseUser user = mAuth.getCurrentUser();

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("Registro", "createUserWithEmail:failure", task.getException());
                            Toast.makeText(RegistroActivity.this, "Error al registrar usuario",
                                    Toast.LENGTH_SHORT).show();

                        }

                        // ...
                    }
                });


    }

}
