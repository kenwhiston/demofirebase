package com.jassgrouptics.loginfirebase;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText  edtCorreo, edtClave;
    private Button btnIniciarSesion, btnCancelar;



    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtCorreo = findViewById(R.id.edtCorreo);
        edtClave = findViewById(R.id.edtClave);

        btnIniciarSesion = findViewById(R.id.btnIniciarSesion);
        btnCancelar = findViewById(R.id.btnCancelar);

        btnIniciarSesion.setOnClickListener(this);
        btnCancelar.setOnClickListener(this);



        context = this;

    }



    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btnIniciarSesion){
            String email, password;
            email = edtCorreo.getText().toString();
            password = edtClave.getText().toString();

            loginIn(email,password);

        }else if(v.getId() == R.id.btnCancelar){
            finish();
        }
    }

    private void loginIn(String email, String password){



    }
}
