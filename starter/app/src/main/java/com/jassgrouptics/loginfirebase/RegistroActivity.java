package com.jassgrouptics.loginfirebase;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class RegistroActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText edtApellidos, edtNombres, edtCorreo, edtClave, edtRepetirClave;
    private Button btnGuardar,btnCancelar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        edtApellidos = findViewById(R.id.edtApellidos);
        edtNombres = findViewById(R.id.edtNombres);
        edtCorreo = findViewById(R.id.edtCorreo);
        edtClave = findViewById(R.id.edtClave);
        edtRepetirClave = findViewById(R.id.edtRepetirClave);

        btnGuardar = findViewById(R.id.btnGuardar);
        btnCancelar = findViewById(R.id.btnCancelar);

        btnGuardar.setOnClickListener(this);
        btnCancelar.setOnClickListener(this);


    }



    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnGuardar:
                String apellidos, nombres, clave, correo;
                apellidos = edtApellidos.getText().toString();
                nombres = edtNombres.getText().toString();
                clave = edtClave.getText().toString();
                correo = edtCorreo.getText().toString();
                registro(apellidos, nombres, correo, clave);
                break;
            case R.id.btnCancelar:

                break;
        }
    }

    private void registro(String apellidos, String nombres, String correo, String clave){




    }

}
